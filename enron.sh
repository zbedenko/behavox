#!/bin/sh
#
# Loading enron data into elasticsearch
#
# Prerequisites: 
# make sure that stream2es utility is present in the path
# install beautifulsoup4 and lxml:
#    sudo easy_install beautifulsoup4
#    sudo easy_install lxml
#
# The mailboxes__jsonify_mbox.py and mailboxes__convert_enron_inbox_to_mbox.py are modified 
# versions of https://github.com/ptwobrussell/Mining-the-Social-Web/tree/master/python_code
#
if [ ! -d enron_mail_20150507 ]; then
    echo "Downloading enron file"
    curl -O -L http://www.cs.cmu.edu/~enron/enron_mail_20150507.tgz
    tar -xzf enron_mail_20150507.tgz
fi
if [ ! -f enron.mbox.json ]; then
    echo "Converting enron emails to mbox format"
    python2.7 mailboxes__convert_enron_inbox_to_mbox.py enron_mail_20150507 > enron.mbox
    echo "Converting enron emails to json format"
    python2.7 mailboxes__jsonify_mbox.py enron.mbox > enron.mbox.json
    rm enron.mbox
fi

echo "Indexing enron emails"
es_host="search-behavox-project-ijbr3oa3pxml65mtafvyw5d7ki.us-east-1.es.amazonaws.com"
curl -XDELETE "$es_host/enron"
curl -XPUT "$es_host/enron" -d '{
    "settings": {
        "index.number_of_replicas": 0,
        "index.number_of_shards": 5
    },
    "mappings": {
        "email": {
            "properties": {
                "Bcc": {
                    "type": "keyword"
                },
                "Cc": {
                    "type": "keyword"
                },
                "Content-Transfer-Encoding": {
                    "type": "keyword"
                },
                "Content-Type": {
                    "type": "keyword"
                },
                "Date": {
                    "type" : "date",
                    "format" : "EEE, d MMM YYYY HH:mm:ss Z"
                },
                "From": {
                    "type": "keyword"
                },
                "Message-ID": {
                    "type": "keyword"
                },
                "Mime-Version": {
                    "type": "keyword"
                },
                "Subject": {
                    "type": "text",
                    "fielddata": true
                },
                "To": {
                    "type": "keyword"
                },
                "X-FileName": {
                    "type": "keyword"
                },
                "X-Folder": {
                    "type": "keyword"
                },
                "X-From": {
                    "type": "keyword"
                },
                "X-Origin": {
                    "type": "keyword"
                },
                "X-To": {
                    "type": "keyword"
                },
                "X-bcc": {
                    "type": "keyword"
                },
                "X-cc": {
                    "type": "keyword"
                },
                "bytes": {
                    "type": "long"
                },
                "offset": {
                    "type": "long"
                },
                "parts": {
                    "dynamic": "true",
                    "properties": {
                        "content": {
                            "type": "text",
			    "fielddata": true

                        },
                        "contentType": {
                            "type": "keyword"
                        }
                    }
                }
            }
        }
    }
}'

curl -XPOST "search-behavox-project-ijbr3oa3pxml65mtafvyw5d7ki.us-east-1.es.amazonaws.com/_bulk" --data-binary @enron.mbox.json


